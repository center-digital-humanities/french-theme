				<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
				// Only run if The Events Calendar is installed 
				if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax()) { 
					// Do nothing	
				}
				// For posts
				elseif (is_single() || is_category() || is_search()) { ?>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<?php else : endif; ?>
					<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'events-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If an People subpage
								if (is_tree(1555) || get_field('menu_select') == "people") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'People', 'bonestheme' ),
									   	'menu_class' => 'people-nav',
									   	'theme_location' => 'people-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>People</h3> <ul>%3$s</ul>'
									));
								}
								// If an Programs subpage
								if (is_tree(3322) || get_field('menu_select') == "programs") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Programs', 'bonestheme' ),
									   	'menu_class' => 'programs-nav',
									   	'theme_location' => 'programs-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Programs</h3> <ul>%3$s</ul>'
									));
								}
								// If a Graduate subpage
								if (is_tree(863) || get_field('menu_select') == "graduate") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Graduate', 'bonestheme' ),
										'menu_class' => 'grad-nav',
										'theme_location' => 'grad-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Graduate</h3> <ul>%3$s</ul>'
									));
								}
								// If an Undergraduate subpage
								if (is_tree(860) || get_field('menu_select') == "undergraduate") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Undergraduate', 'bonestheme' ),
									   	'menu_class' => 'undergrad-nav',
									   	'theme_location' => 'undergrad-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Undergraduate</h3> <ul>%3$s</ul>'
									));
								}
								// If an Courses subpage
								if (is_tree(1214) || get_field('menu_select') == "courses") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Courses', 'bonestheme' ),
									   	'menu_class' => 'courses-nav',
									   	'theme_location' => 'courses-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Courses</h3> <ul>%3$s</ul>'
									));
								}
								// If an Study Abroad subpage
								if (is_tree(939) || get_field('menu_select') == "study") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Study', 'bonestheme' ),
									   	'menu_class' => 'study-nav',
									   	'theme_location' => 'study-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Study Abroad</h3> <ul>%3$s</ul>'
									));
								}
								// If an Resources subpage
								if (is_tree(1809) || get_field('menu_select') == "resources") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Resources', 'bonestheme' ),
									   	'menu_class' => 'resources-nav',
									   	'theme_location' => 'resources-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Resources</h3> <ul>%3$s</ul>'
									));
								}
								// If an Alumni subpage
								if (is_tree(2224) || get_field('menu_select') == "alumni") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Alumni', 'bonestheme' ),
									   	'menu_class' => 'alumni-nav',
									   	'theme_location' => 'alumni-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Alumni & Friends</h3> <ul>%3$s</ul>'
									));
								}
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if (is_tree(9999) || is_search() || is_404() || is_page('contact') || is_page('about') || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
						<?php if ( is_page( 'give' )  ) : ?>
							<?php dynamic_sidebar( 'give-sidebar' ); ?>
						<?php else : ?>
						<?php endif; ?>
					</div>
				</div>
				<?php } ?>