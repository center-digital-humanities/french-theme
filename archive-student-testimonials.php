<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<h1 class="archive-title">
						Student Testimonials
					</h1>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
						
						<section class="entry-content cf"><h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
							<?php the_post_thumbnail( 'student-photo', array('class'=>'alignleft') ); ?>
                            <p>
                            <?php $content = get_the_content();
                                $limit = '40';

                                $trimmed_content = wp_trim_words( $content, $limit, '...' );
                                echo $trimmed_content; 
                            ?>                            
                            </p>
							<a href="<?php the_permalink() ?>" class="btn">Read More</a>
						</section>
					</article>

					<?php endwhile; ?>
					
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<section>
							<p>There is nothing available to show here at this time. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
                                // If an Study Abroad subpage								
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Alumni & Friends', 'bonestheme' ),
									   	'menu_class' => 'alumni-nav',
									   	'theme_location' => 'alumni-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Alumni & Friends</h3> <ul>%3$s</ul>'
									));
							?>
						</nav>
					</div>
				</div>
			</div>

<?php get_footer(); ?>