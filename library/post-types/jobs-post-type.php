<?php
// Courses Post Type Settings

// add custom categories
register_taxonomy( 'opportunities', 
	array('jobs-internships'), /* if you change the name of register_post_type( 'courses', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Opportunity Categories', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Opportunity Category', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Opportunities Categories', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Opportunities Categories', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Opportunities Category', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Opportunities Category:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Opportunities Category', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Opportunities Category', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Opportunities Category', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Opportunities Category Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'opportunities' )
	)
);
		
// let's create the function for the custom type
function jobs_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'jobs-internships', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Jobs and Internships', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Job and Internship', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Jobs and Internships', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Jobs and Internships', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Jobs and Internships', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Jobs and Internships', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Jobs and Internships', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Jobs and Internships', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No testimonials added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all jobs and internships', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 6, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-carrot', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'jobs-internships', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'jobs-internships', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
            'supports' => array( 'title', 'editor', 'revisions', 'author', 'page-attributes', 'thumbnail', 'excerpt' )
		) /* end of options */
	); /* end of register post type */	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'jobs_post_type');
?>