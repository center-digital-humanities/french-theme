<?php
// Courses Post Type Settings

// add custom categories
register_taxonomy( 'testimonial_cat', 
	array('student-testimonials'), /* if you change the name of register_post_type( 'courses', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Testimonial Categories', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Testimonial Category', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Testimonial Categories', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Testimonial Categories', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Testimonial Category', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Testimonial Category:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Testimonial Category', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Testimonial Category', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Testimonial Category', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Testimonial Category Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'testimonials_cat' )
	)
);

// let's create the function for the custom type
function testimonials_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'student-testimonials', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Student Testimonials', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Student Testimonial', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Student Testimonials', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Student Testimonial', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Student Testimonial', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Student Testimonial', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Student Testimonial', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Student Testimonials', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No testimonials added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all student testimonials', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 6, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-megaphone', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'student-testimonial', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'student-testimonials', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
            'supports' => array( 'title', 'editor', 'revisions', 'author', 'page-attributes', 'thumbnail', 'excerpt' )
		) /* end of options */
	); /* end of register post type */	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'testimonials_post_type');
?>