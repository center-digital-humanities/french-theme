<?php
/*
 Template Name: Ph.D Listiing Template
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>                            
								<ul class="phd-list">
                                    <?php if(have_rows('phd_listing')): ?>
                                    <?php while(have_rows('phd_listing')): the_row(); ?>
                                    <?php
                                        $first_name = get_sub_field('first_name');
                                        $last_name = get_sub_field('last_name');
                                        $person_name = $first_name . ' ' . $last_name;
                                        $grad_year = get_sub_field('graduation_year');
                                        $web_link = get_sub_field('personal_website');
                                        $position_title = get_sub_field('position_title');
                                        $employer = get_sub_field('employer');
                                        $location = get_sub_field('location');
                                        $research = get_sub_field('research');
                                        $committee = get_sub_field('committee');


                                    ?>
                                        <li>
                                            <div class="person-info">
                                                <?php if($first_name): ?>
                                                    <h4>		
                                                        <?php if($web_link): ?>
                                                            <a href="<?php echo $web_link; ?>" class="phd-link" target="_blank">
                                                        <?php endif; ?>
                                                            <?php echo $person_name; ?>
                                                        <?php if($web_link): ?>
                                                            </a>
                                                        <?php endif; ?>
                                                    </h4>
                                                <?php endif; ?>

                                                <?php if($position_title): ?>
                                                    <span><strong><?php echo $position_title; ?></strong></span>
                                                <?php endif; ?>
                                                <?php if($employer): ?>
                                                    <span><?php echo $employer; ?></span>
                                                <?php endif; ?>
                                                <?php if($location): ?>
                                                    <span><?php echo $location; ?></span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="research">
                                                        <?php if($grad_year): ?>
                                                           <span class="grad_year"> (<?php echo $grad_year; 
                                                            if($committee){
                                                            echo ", ". $committee;
                                                            }
                                                        ?>)</span>
                                                        <?php endif; ?>
                                                <?php if($research): ?>
                                                    <?php echo $research; ?>
                                                <?php endif; ?>
                                            </div>
                                        </li>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
								</ul>													
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>