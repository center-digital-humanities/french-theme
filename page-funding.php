<?php
/*
 Template Name: Funding Template
*/
?>
<?php get_header(); ?> 
			<div class="content">
				<div class="col" id="main-content" role="main">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
                            
                            <dl class="funding-list">
                                <?php if(have_rows('funding_list')): ?>
                                <?php while(have_rows('funding_list')): the_row(); ?>
                                    <?php
                                        $title = get_sub_field('title');
                                        $description = get_sub_field('description');
                                        $full_story= get_sub_field('full_story');
                                    ?>
                                    <dt class="<?php if($full_story): ?>question<?php else: ?>no-question<?php endif; ?>">
                                        <h4><?php echo $title; ?></h4>
                                        <?php if($description): ?>
                                            <span class="description"><?php echo $description; ?></span>
                                        <?php endif; ?>
                                        <?php if($full_story): ?><a class="view-btn">View More</a><?php endif; ?>
                                    </dt>
                                    <?php if($full_story): ?>
                                        <dd class="answer"><?php echo $full_story; ?></dd>
                                    <?php endif; ?>	
                                <?php endwhile; ?>
                                <?php endif; ?>
                            </dl>					
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>