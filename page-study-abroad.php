<?php
/*
 Template Name: Study Abroad
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
                            
								<ul class="abroad-list">
									<?php $author = get_field('abroad_pages'); ?>
									<? if( $author ): ?>
									<?php foreach( $author as $post): ?>
									<?php setup_postdata($post); ?>
									<li>
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>                                        
                                            <div>
                                                <?php $content = get_the_content();
                                                $trimmed_content = wp_trim_words( $content, 27, '...' );
                                                echo $trimmed_content; ?>
                                            </div>
                                        
                                    </li>
									<?php endforeach; ?>
									<?php// wp_reset_postdata(); ?>
									<?php endif; ?>
								</ul>													
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
                                // If an Study Abroad subpage								
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Study Abroad', 'bonestheme' ),
									   	'menu_class' => 'study-nav',
									   	'theme_location' => 'study-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Study Abroad</h3> <ul>%3$s</ul>'
									));
							?>
						</nav>
					</div>
				</div>
			</div>

<?php get_footer(); ?>