<?php get_header(); ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<header class="bio <?php if(get_field('person_type') == "alumni") { ?>alumni<?php } ?>">
					<div class="content">
						<?php if(get_field('photo')) {
							$image = get_field('photo');
							if( !empty($image) ): 
							// vars
							$url = $image['url'];
							$title = $image['title'];
							// thumbnail
							$size = 'people-large';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];
						endif; ?>
						<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
						<?php } else { ?>
						<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-square-logo-500.jpg" alt="UCLA Logo" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
						<?php } ?>
						<section>
							<h1 id="bio"><?php the_title(); ?></h1>
							<?php if(get_field('position_title')) { ?>
							<span class="position"><?php the_field('position_title'); ?></span>
							<?php } ?>
							<?php if(get_field('person_type') == "alumni") { ?>
							<div class="employment">
								<?php if(get_field('position_title')) { ?>
								<span class="position"><?php the_field('position_title'); ?></span>
								<?php } ?>
								<?php if(get_field('employer')) { ?><span class="employer"> at <?php the_field('employer'); ?></span>
								<?php } ?>
							</div>
							<?php } ?>       
							<div class="details">
							<?php if(get_field('email_address')) { ?>
								<span><strong>E-mail: </strong><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a></span>
							<?php } ?>
							<?php if(get_field('phone_number')) { ?>
								<span><strong>Phone: </strong><?php the_field('phone_number'); ?></span>
							<?php } ?>
							<?php if(get_field('office')) { ?>
								<span><strong>Office: </strong><?php the_field('office'); ?></span>
							<?php } ?>
							<?php if(get_field('office_hours')) { ?>
								<p><strong>Office Hours: </strong><?php the_field('office_hours'); ?></p>
							<?php } ?>
							</div>
                            <?php if(get_field('interest')) { ?>
                                <div class="interest">
                                   <strong>Research Interest: </strong> <?php the_field('interest'); ?>
                                </div>
                            <?php } ?>
                            <?php if(get_field('facebook_url') || get_field('twitter_url') || get_field('instagram_url') || get_field('google_url') || get_field('linkedin_url') || get_field('youtube_url')) { ?>
                                <ul class="social-links">
                                <?php if(get_field('facebook_url')) { ?>
                                    <li class="icon facebook"><a href="<?php the_field('facebook_url'); ?>"><span class="fa fa-facebook-square" aria-hidden="true"><span class="hidden">Facebook</span></span></a></li>
                                <?php } if(get_field('twitter_url')) { ?>
                                    <li class="icon twitter"><a href="<?php the_field('twitter_url'); ?>"><span class="fa fa-twitter-square" aria-hidden="true"><span class="hidden">Twitter</span></span></a></li>
                                <?php } if(get_field('instagram_url')) { ?>
                                    <li class="icon instagram"><a href="<?php the_field('instagram_url'); ?>"><span class="fa fa-instagram" aria-hidden="true"><span class="hidden">Instagram</span></span></a></li>
                                <?php } if(get_field('google_url')) { ?>
                                    <li class="icon google"><a href="<?php the_field('google_url'); ?>"><span class="fa fa-google-plus-square" aria-hidden="true"><span class="hidden">Google Plus</span></span></a></li>
                                <?php } if(get_field('linkedin_url')) { ?>
                                    <li class="icon linkedin"><a href="<?php the_field('linkedin_url'); ?>"><span class="fa fa-linkedin-square" aria-hidden="true"><span class="hidden">Linkedin</span></span></a></li>
                                <?php } if(get_field('youtube_url')) { ?>
                                    <li class="icon youtube"><a href="<?php the_field('youtube_url'); ?>"><span class="fa fa-youtube-square" aria-hidden="true"><span class="hidden">YouTube</span></span></a></li>
                                <?php } ?>
                                </ul>
                            <?php } ?>
                            
							<?php the_content(); ?>
						</section>
					</div>
				</header>
				<div class="content main">
					<div class="col" id="main-content" role="main">
						<?php if(get_field('education')) { ?>
						<section id="education">
							<h2>Education</h2>
							<?php the_field('education'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('courses')) { ?>
						<section id="courses">
							<h2>Courses Commonly Taught</h2>
							<?php the_field('courses'); ?>
						</section>
						<?php } ?>
							<?php if(get_field('featured_works')) { ?>
							<section id="featured_works">
								<h2>Featured Works</h2>
								<?php $book = get_field('featured_works'); ?>
								<ul class="book-list">
									<? if( $book ): ?>
									<?php foreach( $book as $post): ?>
									<?php setup_postdata($post); ?>
									<li>
                                        <?php if(get_field('external_url')){ ?>
                                            <a href="<?php the_field('external_url'); ?>" title="<?php the_title_attribute(); ?>" target="_blank">
                                        <?php }else{ ?>
                                            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                                        <?php } ?>
										<?php if(get_field('book_cover')) {
											$image = get_field('book_cover');
											if( !empty($image) ): 
												// vars
												$url = $image['url'];
												$title = $image['title'];
												// thumbnail
												$size = 'small-book';
												$thumb = $image['sizes'][ $size ];
												$width = $image['sizes'][ $size . '-width' ];
												$height = $image['sizes'][ $size . '-height' ];
											endif; ?>
											<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> book cover" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="cover" />
											<?php } else { ?>
												<div class="custom-cover cover">
													<span class="title"><?php the_title(); ?></span>
												</div>
											<?php } ?>
										</a>
										<dl>
											<dt class="title">
                                                <?php if(get_field('external_url')){ ?>
                                                <a href="<?php the_field('external_url'); ?>" title="<?php the_title_attribute(); ?>" target="_blank">
                                                <?php }else{ ?>
                                                    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">                    
                                                <?php } ?>
                                                    <?php the_title(); ?></a>  
                                                
												
											</dt>
											<?php if(get_field('subtitle')) { ?>
											<dd class="subtitle">
												<?php the_field('subtitle'); ?>
											</dd>
											<?php } ?>
											<?php if(get_field('publisher')) { ?>
											<dd class="publisher">
												<?php the_field('publisher'); ?>, <?php the_field('published_date'); ?>
											</dd>
											<?php } ?>
										</dl>
									</li>
									<?php endforeach; ?>
									<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</ul>
							</section>
							<?php } ?>
						<?php if(get_field('books') || get_field('articles') || get_field('edited_volumes') || get_field('additional_works') || get_field('reviews')) { ?>
						<section id="publications">
							<h2>Selected Publications</h2>
                            <?php if(get_field('books')) { ?>
                            <section id="books">
                                <h3>Books</h3>
                                <?php the_field('books'); ?>
                            </section>
                            <?php } ?>                            
							<?php if(get_field('articles')) { ?>
							<section id="articles">
								<h3>Articles</h3>
							<?php the_field('articles'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('reviews')) { ?>
							<section id="reviews">
								<h3>Reviews</h3>
								<?php the_field('reviews'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('edited_volumes')) { ?>
							<section id="edited_volumes">
								<h3>Edited Volumes</h3>
                                <?php the_field('edited_volumes'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('translations')) { ?>
							<section id="translations">
								<h3>Translations</h3>
                                <?php the_field('translations'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('additional_works')) { ?>
							<section id="works">
								<?php the_field('additional_works'); ?>
							</section>
							<?php } ?>
						</section>
						<?php } ?>
						<?php if(get_field('honors_and_awards')) { ?>
						<section id="honors_and_awards">
							<h2>Honors and Awards</h2>
							<?php the_field('honors_and_awards'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('professional_activities')) { ?>
						<section id="professional_activities">
							<h2>Professional Activities</h2>
							<?php the_field('professional_activities'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('positions_held')) { ?>
						<section id="positions_held">
							<h2>Positions Held</h2>
							<?php the_field('positions_held'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('custom_section_title')) { ?>
						<section id="other">
							<h2><?php the_field('custom_section_title'); ?></h2>
							<?php the_field('custom_section_content'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('custom_section_title_2')) { ?>
						<section id="other2">
							<h2><?php the_field('custom_section_title_2'); ?></h2>
							<?php the_field('custom_section_content_2'); ?>
						</section>
						<?php } ?>
					</div>
					<?php 
					// Make nav appear if only if there is anything to show
					if(get_field('featured_works') || get_field('professional_activities') || get_field('books') || get_field('articles') || get_field('additional_works') || get_field('reviews') || get_field('positions_held') || get_field('honors_and_awards') || get_field('courses') || get_field('custom_section_title') || get_field('cv') || get_field('personal_website') || get_field('academia_profile') || get_field('additional_link')) { ?>
					<div class="col person-nav nav-container">
						<div class="content col-nav">
							<nav role="navigation" aria-labelledby="person navigation">
								<ul class="table-of-contents">
									<h3>About <?php the_field('first_name'); ?></h3>
									<?php if( empty( $post->post_content) ) {
									// If there is no bio, don't show bio link
									} else { ?>
									<li><a href="#bio">Background</a></li>
									<?php } ?>
									<?php if(get_field('education')) { ?>
									<li><a href="#professional_activities">Education</a></li>
									<?php } ?>
									<?php if(get_field('courses')) { ?>
									<li><a href="#courses">Courses Commonly Taught</a></li>
									<?php } ?>
									<?php if(get_field('featured_works')) { ?>
									<li><a href="#featured_works">Featured Works</a></li>
									<?php } ?>
									<?php if(get_field('books') || get_field('articles') || get_field('additional_works') || get_field('reviews') || get_field('translations') || get_field('edited_volumes')) { ?>
									<li><a href="#publications">Selected Publications</a></li>
									<?php } ?>
									<?php if(get_field('honors_and_awards')) { ?>
									<li><a href="#honors_and_awards">Honors & Awards</a></li>
									<?php } ?>
									<?php if(get_field('professional_activities')) { ?>
									<li><a href="#professional_activities">Professional Activities</a></li>
									<?php } ?>
									<?php if(get_field('positions_held')) { ?>
									<li><a href="#positions_helds">Positions Held</a></li>
									<?php } ?>
									<?php if(get_field('custom_section_title')) { ?>
									<li><a href="#other"><?php the_field('custom_section_title'); ?></a></li>
									<?php } ?>
									<?php if(get_field('custom_section_title_2')) { ?>
									<li><a href="#other2"><?php the_field('custom_section_title_2'); ?></a></li>
									<?php } ?>
									<?php if(get_field('cv')) { ?>
									<li><a href="<?php the_field('cv'); ?>" class="download">Download CV</a></li>
									<?php } ?>
									<?php if(get_field('personal_website')) { ?>
									<li><a href="<?php the_field('personal_website'); ?>" class="link">Personal Website</a></li>
									<?php } ?>
									<?php if(get_field('academia_profile')) { ?>
									<li><a href="<?php the_field('academia_profile'); ?>" class="link">Academia Profile</a></li>
									<?php } ?>
									<?php if(get_field('additional_link')) { ?>
									<li><a href="<?php the_field('additional_link'); ?>" class="link"><?php the_field('additional_link_title'); ?></a></li>
									<?php } ?>
								</ul>
							</nav>
						</div>
					</div>
					<?php } ?>
				</div>
			<?php endwhile; ?>
			<?php else : endif; ?>
<?php get_footer(); ?>