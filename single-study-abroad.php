<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
						<section class="entry-content cf" itemprop="articleBody">
							<?php the_post_thumbnail( 'content-width' ); ?>
							<?php the_content(); ?>
						</section>                    
                        <?php if(get_field('eligibilty_field')) { ?>
                            <section class="eligibilty">
                               <h3>Eligibilty </h3> 
                                <?php the_field('eligibilty_field'); ?>
                            </section>
                        <?php } ?>
                        <?php if(get_field('scholarships_field')) { ?>
                            <section class="scholarships">
                               <h3>Scholarships </h3> 
                                <?php the_field('scholarships_field'); ?>
                            </section>
                        <?php } ?>
                        <?php if(get_field('application_field')) { ?>
                            <section class="application">
                               <h3>Application</h3> 
                                <?php the_field('application_field'); ?>
                            </section>
                        <?php } ?>
                        <?php if(get_field('deadline_field')) { ?>
                            <section class="deadline_field">
                               <h3>Deadline</h3> 
                                <?php the_field('deadline_field'); ?>
                            </section>
                        <?php } ?>
                        
                        
                                            
                        <?php if(get_field('dates_and_location')) { ?>
                            <section class="dates_and_location">
                               <h3>Dates and Location</h3> 
                                <?php the_field('dates_and_location'); ?>
                            </section>
                        <?php } ?>
                        <?php if(get_field('faculty_field')) { ?>
                            <section class="faculty_field">
                               <h3>Faculty</h3> 
                                <?php the_field('faculty_field'); ?>
                            </section>
                        <?php } ?>
                        <?php if(get_field('links_field')) { ?>
                            <section class="links">
                               <h3>Link</h3> 
                                <?php the_field('links_field'); ?>
                            </section>
                        <?php } ?>
                        <?php if(get_field('contact_field')) { ?>
                            <section class="contact_field">
                               <h3>Contact</h3> 
                                <?php the_field('contact_field'); ?>
                            </section>
                        <?php } ?>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>
				
				</div>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
                                // If an Study Abroad subpage								
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Study Abroad', 'bonestheme' ),
									   	'menu_class' => 'study-nav',
									   	'theme_location' => 'study-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Study Abroad</h3> <ul>%3$s</ul>'
									));
							?>
						</nav>
					</div>
				</div>
			</div>

<?php get_footer(); ?>