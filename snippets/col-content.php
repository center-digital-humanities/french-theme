<div class="col content-col <?php the_sub_field('content_width'); ?>">
	<h3><?php the_sub_field('content_title'); ?></h3>
    
    <?php 
        if(get_sub_field('show_image') == "yes") {
            if(get_sub_field('featured_image')) {
            $image = get_sub_field('featured_image');
            if( !empty($image) ): 
            // vars
            $url = $image['url'];
            $title = $image['title'];
            // thumbnail
            $size = 'article-lrg-thumb';
            $thumb = $image['sizes'][ $size ];
            $width = $image['sizes'][ $size . '-width' ];
            $height = $image['sizes'][ $size . '-height' ];
        endif; ?>
         
        <?php if(get_sub_field('show_button') == "yes") { ?>
            <a href="<?php the_sub_field('button_link'); ?><?php if(get_sub_field('page_anchor')){ echo '#'. get_sub_field('page_anchor'); } ?>"> 
        <?php } ?>
        <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo wp-post-image"/>
        <?php if(get_sub_field('show_button') == "yes") { ?>
             </a>
        <?php } ?>
        <?php } 
        } 
    ?>    
    
	<?php the_sub_field('content'); ?>
	<?php if(get_sub_field('show_button') == "yes") { ?>
	<a class="btn" href="<?php the_sub_field('button_link'); ?><?php if(get_sub_field('page_anchor')){ echo '#'. get_sub_field('page_anchor'); } ?>"><?php the_sub_field('button_text'); ?></a>
	<?php } ?>
</div>