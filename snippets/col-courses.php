<?php /*-------------[ COURSES }------------------------*/?>

<div class="col courses <?php the_sub_field('content_width'); ?>">
    <?php $course_link = get_sub_field('button_link'); ?>
    <h3><?php the_sub_field('courses_title'); ?></h3>
     <?php if(get_sub_field('courses_description')) { ?>
        <?php the_sub_field('courses_description'); ?>
    <?php } ?>
    <ul>
        <?php $course = get_sub_field('courses'); ?>
        <? if( $course ): ?>
        <?php foreach( $course as $post): ?>
        <?php setup_postdata($post); ?>
        <?php 
            $short_description = get_field('short_description');
            $content = get_the_content();
            $trimmed_content = wp_trim_words( $content, 15, '...' );
        ?>

        <li>
            <dl>
                <dt class="title">
                    <h4><?php the_title(); ?></h4>
                </dt>
                <dd class="instructors">
                    <?php if(get_field('instructor_type') == "internal") { ?>
                        <strong>Instructor: </strong>
                        <?php $instructor = get_field('instructor'); ?>
                        <?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        <?php endif; ?>
                    <?php } if(get_sub_field('instructor_type') == "both") { ?>
                    <strong>Instructor: </strong>
                    <?php $instructor = get_field('instructor'); ?>
                    <?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    <?php endif; ?>
                    <?php }
                    if(get_field('instructor_type') == "external") { ?>
                        <?php if(get_field('additional_instructors')) { ?>
                        <strong>Instructor: </strong><?php the_field('additional_instructors'); ?>
                        <?php } ?>
                    <?php }	?>
                </dd>
                <?php wp_reset_postdata(); ?>
                <dd class="short-description">
                    <?php if($short_description) { ?>
                        <?php echo $short_description ?>
                    <?php } else { ?>
                        <p>
                            <?php echo $trimmed_content; ?>
                        </p>
                    <?php } ?>
                </dd>
            </dl>
        </li>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </ul>
    <?php if(get_sub_field('show_button') == "yes") { ?>
        <div class="view-all-container">
           <a class="btn" href="<?php the_sub_field('button_link'); ?>">&raquo; <?php the_sub_field('button_text'); ?></a>
        </div>
	<?php } ?>
</div>