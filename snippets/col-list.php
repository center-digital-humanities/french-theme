<div class="col pagelist-col one">
        <?php $course_link = get_sub_field('listing_page_link'); ?>
        <h3><?php the_sub_field('listing_title'); ?></h3>
         <?php if(get_sub_field('listing_description')) { ?>
            <?php the_sub_field('listing_description'); ?>
        <?php } ?>
        <ul>
            <?php $section = get_sub_field('listing'); ?>
            <? if( $section ): ?>
            <?php foreach( $section as $post): ?>
            <?php setup_postdata($post); ?>
            <?php 
                $short_description = get_sub_field('short_description');
                $content = get_the_content();
                $trimmed_content = wp_trim_words( $content, 15, '...' );
            ?>
            <li>
                <dl>
                    <dt class="title">
                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                    </dt>
                    
                    <dd class="short-description">
                        <?php if($short_description) { ?>
                            <?php echo $short_description ?>
                        <?php } else { ?>
                            <p>
                                <?php echo $trimmed_content; ?>
                                <a href="<?php the_permalink(); ?>">Read More</a>
                            </p>
                        <?php } ?>
                    </dd>
                </dl>
            </li>
            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </ul>
        <div class="view-all-container">
            <a class="btn" href="<?php echo $section_page_link ?>">View All</a>
        </div>
</div>