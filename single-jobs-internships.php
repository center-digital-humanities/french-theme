<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
						<section class="entry-content cf" itemprop="articleBody">
							<?php the_post_thumbnail( 'content-width' ); ?>
							<?php the_content(); ?>
						</section>                    
                        <?php if(get_field('weblink')) { ?>
                            <section class="weblink">
                                <a href="<?php the_field('weblink'); ?>" class="btn">Visit Website</a>
                            </section>
                        <?php } ?>
                        
                        
                        
                        
                        
                        
                        
							

                        <?php if(have_rows('contact_field')): ?>
                                <section class="contact_field">
                                   <h4>Contact</h4>
							<?php while(have_rows('contact_field')): the_row(); ?>                                    
                                    <div class="contain-col">
                                    <?php if(get_sub_field('contact_name')){ 
                                            the_sub_field('contact_name');
                                            echo '<br>';
                                        } ?>
                                    <?php if(get_sub_field('organization')){ 
                                            the_sub_field('organization');
                                            echo '<br>';
                                        } ?>
                                    <?php if(get_sub_field('location')){ 
                                            the_sub_field('location');
                                            echo '<br>';
                                        } ?>
                                    <?php if(get_sub_field('city')){ 
                                            the_sub_field('city');
                                            echo ', ';
                                        } ?>
                                    <?php if(get_sub_field('state_province')){ 
                                            the_sub_field('state_province');
                                            echo ' ';
                                        } ?>
                                    <?php if(get_sub_field('zip_postal_code')){ 
                                            the_sub_field('zip_postal_code');
                                            echo '<br>';
                                        } ?>
                                    <?php if(get_sub_field('phone')){ ?>
                                            <strong>Phone: </strong>
                                           <?php the_sub_field('phone');
                                            echo '<br>';
                                        } ?>
                                    <?php if(get_sub_field('email')) { ?>
                                        <span><strong>E-mail: </strong>
                                            <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></span><br>
                                    <?php } ?>
                                    <?php if(get_sub_field('contact_website_1')){ 
                                            the_sub_field('contact_website_1');
                                            echo '<br>';
                                        } ?>
                                    <?php if(get_sub_field('contact_website_2')){ 
                                            the_sub_field('contact_website_2');
                                            echo '<br>';
                                        } ?>
                                    <?php if(get_sub_field('contact_website_3')){ 
                                            the_sub_field('contact_website_3');
                                            echo '<br>';
    
                                        } ?>
                                    </div>
                            <?php endwhile; ?>
                                </section>
                        <?php endif; ?>
                        
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>
				
				</div>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
                                // If an Study Abroad subpage								
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Resources', 'bonestheme' ),
									   	'menu_class' => 'resources-nav',
									   	'theme_location' => 'resources-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Resources</h3> <ul>%3$s</ul>'
									));
							?>
						</nav>
					</div>
				</div>
			</div>

<?php get_footer(); ?>