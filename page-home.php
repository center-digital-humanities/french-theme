<?php
/*
 Template Name: Home Page
*/
?>
<?php get_header(); ?>
			<div id="main-content" role="main">
				<?php // If set to Single/Random 
					if(get_field('hero_type', 'option') == "single") {
					$rows = get_field('hero_image'); // get all the rows
					$rand_row = $rows[ array_rand($rows) ]; // get a random row
					$silder_image = $rand_row['image']; // get the sub field value
					$slider_title = $rand_row['title']; // get the sub field value 
					$slider_description = $rand_row['description']; // get the sub field value 
					$slider_button = $rand_row['button_text']; // get the sub field value 
					$slider_link = $rand_row['link']; // get the sub field value 			
					if(!empty($silder_image)): 
						// vars
						$url = $silder_image['url'];
						$title = $silder_image['title'];
						// thumbnail
						$size = 'home-hero';
						$slide = $silder_image['sizes'][ $size ];
						$width = $silder_image['sizes'][ $size . '-width' ];
						$height = $silder_image['sizes'][ $size . '-height' ];                    
                        $caption = $silder_image['caption'];
					endif;
				?>
				<?php if($silder_image): ?>
				<?php if($slider_link): ?>
				<a href="<?php echo $slider_link; ?>" class="hero-link">
				<?php endif; ?>
					<div id="hero" class="desktop" style="background-image: url('<?php echo $slide; ?>');">
						<div class="content <?php if($slider_title || $slider_description): ?>text<?php endif; ?>">
						<?php if($slider_title || $slider_description): ?>
							<div class="hero-description <?php the_field('vertical_text_alignment', 'option'); ?>">
								<div class="content <?php the_field('horizontal_text_alignment', 'option'); ?>">
									<h2><?php echo $slider_title; ?></h2>
									<?php if($slider_description): ?>
									<p><?php echo $slider_description; ?></p>
									<?php endif; ?>
									<?php if($slider_button): ?>
									<button class="outline"><?php echo $slider_button; ?></button>
									<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>
                            
                            <div class="content">
                               <?php   //This displays the caption below the featured image
                                    if ( $caption ) : ?>
                                        <div class="image-caption-container">
                                            <p class="caption"><?php echo $caption; ?></p>
                                        </div>
                            <?php endif; ?>
                            </div> 
						</div>
					</div>
					<div id="hero" class="mobile-hero" style="background-image:url('<?php echo $slide; ?>');">                                    
                        <?php   //This displays the caption below the featured image
                            if ( $caption ) : ?>
                                <div class="image-caption-container">
                                    <p class="caption"><?php echo $caption; ?></p>
                                </div>
                        <?php endif; ?>
					</div>
				<?php if($slider_link): ?>
				</a>
				<?php endif; ?>
				<?php endif; ?>
				<?php } ?>
				
				<?php // If set to Slider
					if(get_field('hero_type', 'option') == "slider") { ?>
					<script type="text/javascript">
						jQuery("document").ready(function($) {
							$(document).ready(function(){
							  $('#bxslider').bxSlider({
							  	autoHover: true,
							  	auto: false,
							  });
							});
						});
					</script>
					<div id="slider">
						<ul id="bxslider">
							<?php if(have_rows('hero_image')): ?>
							<?php while(have_rows('hero_image')): the_row(); ?>
							<?php
								$slider_title = get_sub_field('title');
								$slider_description = get_sub_field('description');
								$slider_link = get_sub_field('link');
								$silder_image = get_sub_field('image');
								if(!empty($silder_image)): 
									// vars
									$url = $silder_image['url'];
									$title = $silder_image['title'];
									// thumbnail
									$size = 'home-hero';
									$slide = $silder_image['sizes'][ $size ];
									$width = $silder_image['sizes'][ $size . '-width' ];
									$height = $silder_image['sizes'][ $size . '-height' ];
                                    $caption = $silder_image['caption'];
								endif;
							?>		
							<?php if($slider_link): ?>
							<a href="<?php echo $slider_link; ?>" class="hero-link">
							<?php endif; ?>
								<li style="background-image: url('<?php echo $slide; ?>');">
									<div class="content">
										<div class="slider-content">
											<?php if($slider_title): ?>
											<h2><?php echo $slider_title; ?></h2>
											<?php endif; ?>
											<?php if($slider_description): ?>
											<p><?php echo $slider_description; ?></p>
											<?php endif; ?>
										</div>     
                                        <?php //This displays the featured image Captions
                                            if ( $caption ) : ?>
                                                <div class="image-caption-container">
                                                    <p class="caption"><?php echo $caption; ?></p>
                                                </div>
                                        <?php endif; ?>                                        
									</div>
								</li>
							<?php if($slider_link): ?>
							</a>
							<?php endif; ?>
							<?php endwhile; ?>
							<?php endif; ?>
						</ul>
					</div>
				<?php } ?>
                <div class="top-container" >
                    <div class="content">
                        <?php
                        if(have_rows('homepage_columns')) :
                            while (have_rows('homepage_columns')) : the_row();

                                // For showing snippet from any page
                                if(get_row_layout() == 'page_excerpt') 
                                    get_template_part('snippets/col', 'page');

                                // For showing list of recent post
                                elseif(get_row_layout() == 'recent_posts') 
                                    get_template_part('snippets/col', 'posts');

                                // For showing free form content
                                elseif(get_row_layout() == 'content_block') 
                                    get_template_part('snippets/col', 'content');

                                // For showing list of events from event widget
                                elseif(get_row_layout() == 'upcoming_events') 
                                    get_template_part('snippets/col', 'events');                                

                                // For showing list of pages
                                elseif(get_row_layout() == 'page_list') 
                                    get_template_part('snippets/col', 'list');

                                // For showing a menu
                                elseif(get_row_layout() == 'menu') 
                                        get_template_part('snippets/col', 'menu');

                                // For showing courses
                                elseif(get_row_layout() == 'courses_block') 
                                        get_template_part('snippets/col', 'courses');

                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
                <div class="testimonials">
                    <div class="students-col <?php the_field('students_post_width'); ?>">
                        <div class="content">
                        <h3><?php the_field('students_title'); ?></h3>
                        <?php $term = get_sub_field('students_category');
                            $amount = get_field('students_amount_to_show');
                            $posts_query = new WP_Query( array( 'post_type' => 'student-testimonials', 'showposts' => $amount, 'posts_per_page' => 1, 'cat' => $term->term_id, 'orderby' => 'rand' ) ); ?>
                        <ul <?php if(get_field('students_show_categories') == "yes") { ?> class="categories" <?php } ?>>
                            <?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>
                            <?php if(get_field('students_show_categories') == "no") { ?><a href="<?php the_permalink() ?>"><?php } ?>
                                <li>
                                    <?php  

                                       // if(get_field('students_post_width') == "two") { 
                                          //  if(get_field('students_show_image') == "yes") { 
                                                if(get_field('students_show_categories') == "yes") { ?>
                                                    <a href="<?php the_permalink() ?>">
                                                <?php }
                                                    if ( has_post_thumbnail() ) {
                                                        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'student-photo' );
                                                        $url = $thumb['0']; ?>
                                                        <img src="<?=$url?>" alt="<?php the_title(); ?>" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-thumb.jpg" alt="A photo of <?php the_title(); ?>" />
                                                <?php }
                                                if(get_field('students_show_categories') == "yes") { ?>
                                                    </a>
                                                <?php }
                                           // }
                                        //} 
                                    ?>
                                    <div class="item">
                                    <?php if(get_field('students_show_categories') == "yes") { ?>
                                        <span class="category"><?php the_category( ', ' ); ?></span>
                                    <?php } ?>
                                        <?php if(get_field('students_show_categories') == "yes") { ?><a href="<?php the_permalink() ?>"><?php } ?>
                                        <p>
                                            <?php $content = get_the_content();
                                            $trimmed_content = wp_trim_words( $content, 50, '... ' );
                                            echo $trimmed_content; ?>
                                            <a href="<?php the_permalink() ?>">Read more</a> 
                                        </p>
                                        
                                        <!-- a href="<?php the_permalink() ?>"><h4><?php the_title(); ?></h4></a /-->
                                        <?php if(get_field('students_show_categories') == "yes") { ?></a><?php } ?>
                                    </div>
                                </li>
                            <?php if(get_field('students_show_categories') == "no") { ?></a><?php } ?>
                            <?php endwhile; ?>
                        </ul>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                        
                        <div class="view-btn"><?php $terms_student = get_field('students_category'); ?>
                            <?php if( $terms_student ) { ?>
                            <a class="btn" href="/category/<?php echo $terms_student->slug; ?>/">View All<span class="hidden"> <?php echo $terms_student->name; ?></span></a>
                            <?php } else { ?>
                            <a class="btn" href="/student-testimonials/">View All<span class="hidden">Student Testimonials</span></a>
                            <?php } ?>                    
                        </div>
                        </div>
                    </div>
                    
                    <div id="hero" class="desktop" style="background-image:linear-gradient(to right, rgba(11,79,108,1) 0%,rgba(11,79,108,0.52) 20%,rgba(11,79,108,0.09) 38%,rgba(11,79,108,0) 42%), url('<?=$url?>');"></div>
                </div>
                <div class="bottom-container">
                    <div class="content">
                        <?php
                        if(have_rows('homepage_bottom_columns')) :
                            while (have_rows('homepage_bottom_columns')) : the_row();

                                // For showing snippet from any page
                                if(get_row_layout() == 'page_excerpt') 
                                    get_template_part('snippets/col', 'page');

                                // For showing list of recent post
                                elseif(get_row_layout() == 'recent_posts') 
                                    get_template_part('snippets/col', 'posts');

                                // For showing free form content
                                elseif(get_row_layout() == 'content_block') 
                                    get_template_part('snippets/col', 'content');

                                // For showing list of events from event widget
                                elseif(get_row_layout() == 'upcoming_events') 
                                    get_template_part('snippets/col', 'events');                                

                                // For showing list of pages
                                elseif(get_row_layout() == 'page_list') 
                                    get_template_part('snippets/col', 'list');

                                // For showing a menu
                                elseif(get_row_layout() == 'menu') 
                                        get_template_part('snippets/col', 'menu');

                                // For showing courses
                                elseif(get_row_layout() == 'courses_block') 
                                        get_template_part('snippets/col', 'courses');

                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
			</div>
		</div>
<?php get_footer(); ?>